﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialCSharp1
{
    public class StudentClass1
    {
        // creating two variables and value as empty by defualt
        string Firstname = string.Empty;
        string Lastname = string.Empty;


        //lets create constructor as studentClass1() without any parameter
        public StudentClass1()
        {
            //An empty constructor
        }


        // creating constroctor with one parameter
        public StudentClass1(string Fname)
        {
            Firstname = Fname;
        }


        //creating constructor with two parameters
        public StudentClass1(string Fname, string Lname)
        {
            Firstname = Fname;
            Lastname = Lname;
        }


        ////////////////////////////////////////////End of constructors/////////////////////////////////////////////


        /////LETS CREATE METHODS/////
        
        public void displayOutfl()
        {
           Console.WriteLine("firstname:" + Firstname + "lastname:" + Lastname);
        }

        public string displayValfl()
        {
            string display = Firstname + " " + Lastname;
            return display;
        }
    }
}
