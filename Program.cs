﻿

//without any parameters we declaring this method inside StudentClass1
TutorialCSharp1.StudentClass1 obj = new TutorialCSharp1.StudentClass1();
obj.displayOutfl();
string value = obj.displayValfl();
Console.WriteLine(value);

//without any parameters we declaring this method inside StudentClass1
TutorialCSharp1.StudentClass1 obj1 = new TutorialCSharp1.StudentClass1("Adarsh");
obj.displayOutfl();
string value1 = obj1.displayValfl();
Console.WriteLine(value1);

//without any parameters we declaring this method inside StudentClass1
TutorialCSharp1.StudentClass1 obj2 = new TutorialCSharp1.StudentClass1("Adarsh", "mh");
obj.displayOutfl();
string value2 = obj2.displayValfl();
Console.WriteLine(value2);
